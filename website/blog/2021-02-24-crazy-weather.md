---
title: Crazy Weather
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

One day a family was experiencing some crazy weather. On the first day, they had a tornado. On the second day, they had a heavy thunderstorm.

"What is going on?" said the dad. "I don't know" said mom.

It turns out that global warming was hitting in a big way. We had better call Elon Musk.
