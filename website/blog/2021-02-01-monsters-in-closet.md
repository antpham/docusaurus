---
title: Monsters in Closet
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

There once were many monsters under the bed and the children of the house gave them names. As the children grew older, they grew wiser and realized that the monsters had needs too.

The children made friends with the monsters and eventually set the monsters to college. These monsters went to Monsters University.

The end.
