---
title: European Traditions
author: Julian Bede
authorURL: http://twitter.com/ericnakagawa
authorFBID: 661277173
---

European culture is very versatile. You have different customs, traditions and languages, sometimes only kilometers apart from each other.

But now that we have the European Union we are all one big happy family. The end.
