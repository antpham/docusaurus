---
title: Sales Stories
author: Eric Nakagawa
authorURL: http://twitter.com/ericnakagawa
authorFBID: 661277173
---

One day I sold ten things. The next four days I sold nothing.

How do I achieve linearity?
