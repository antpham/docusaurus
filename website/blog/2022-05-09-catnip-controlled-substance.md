---
title: Catnip Rumored to be Next Controlled Susbstance
author: Jordan Van Hoy
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Few could have predicted the newest initiative brought forward with bipartisan support to label catnip as a controlled substance. Feline voters are split on support of this issue given the stipulation that pranks utilizing cucumbers to scare cats would also be listed as a hate crime in this same initiative. Few sources believe this all to be an elaborate, late April Fool's Day prank put on by dog-loving politicians. 
