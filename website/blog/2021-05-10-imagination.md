---
title: Imagination
author: Christopher Wang
authorURL: http://twitter.com/ericnakagawa
authorFBID: 661277173
---

When I was a child, I imagined all of the time. My parents weren't around, so I created my own little world.

One day I will return to imagining with my father who is kind and good to me. What can the world be like if we partnered with each other?
