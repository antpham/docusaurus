---
title: Vegetables Are Awesome
author: Whitney Hurston
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Hey kids, have you heard of broccoli?  How about cauliflower?  Bell peppers ringing a bell?  Kids, let me tell you, vegetables are awesome.  They are totally dope and all the kids these days are really loving showing off their new vegetable eating skills to all the other 5th graders on TikTok.  If you want to go viral, you should definitely have your mom roast up some rad radishes, some bomb brussels sprouts, and some killer carrots!

The end.
