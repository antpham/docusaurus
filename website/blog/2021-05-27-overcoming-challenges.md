---
title: Overcoming Challenges
author: Christopher Wang
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

You don't know what life is going to serve you, therefore children need to grow up sometimes fast!

Tommy was a child who grew up in the inner city in Detroit. He had to learn hope and perseverance, mostly through watching his grandmother.

What are going to show strong character to the world's next generation of children?
